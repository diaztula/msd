'''
This code computes the minimum string distance between 2 strings, and also the mean size of 
all aligments between the two strings.
Implementation is based on the papers:
    1. "Measuring Errors in Text Entry Tasks: An Application of the Levenshtein String Distance Statistic"
    2. "A Character-level Error Analysis Technique for Evaluating Text Entry Methods"
'''

class Alignment(object):
    def __init__(self):
        self.n = 0
        self.sum = 0

def meanAlign(A, B, D, x, y, pathSize, alignment):
    '''Compute the mean alignment between strings A and B, with MSD table D. For more details about this function call see reference 2: A Character-level Error Analysis Technique for Evaluating Text Entry Methods'''
    if x == 0 and y == 0:
        alignment.n += 1
        alignment.sum += pathSize
        return
    if x > 0 and y > 0:        
        if D[x][y] == D[x-1][y-1] and A[x] == B[y]:
            meanAlign(A, B, D, x-1, y-1, pathSize+1, alignment)
            
        if D[x][y] == D[x-1][y-1] + 1:
            meanAlign(A, B, D, x-1, y-1, pathSize+1, alignment)
            
    if x > 0 and D[x][y] == D[x-1][y] + 1:
        meanAlign(A, B, D, x-1, y, pathSize+1, alignment)
        
    if y > 0 and D[x][y] == D[x][y-1] + 1:
        meanAlign(A, B, D, x, y-1, pathSize+1, alignment)

def MSD(s1,s2):
    '''Compute the minumum string distance between s1 and s2. 
    Return value: a tuple containing:
        the valur of msd(s1, s2)
        the mean aligment size meanAlignmentSize(s1, s2)
    '''
    len1 = len(s1) # vertically
    len2 = len(s2) # horizontally
    # Allocate the table
    table = [None]*(len1+1)
    for i in range(len1+1):
        table[i] = [0]*(len2+1)
    # Initialize the table
    for i in range(1, len1+1):
        table[i][0] = i
    for i in range(1, len2+1):
        table[0][i] = i
    # Fill in the table using dynamic programming
    for i in range(1,len1+1):
        for j in range(1,len2+1):
            if s1[i-1] == s2[j-1]:
                d = 0
            else:
                d = 1
            table[i][j] = min(table[i-1][j-1] + d, table[i-1][j]+1, table[i][j-1]+1)

    aligment = Alignment()
    meanAlign(" "+s1, " "+s2, table, len(s1), len(s2), 0, aligment)
    meanAlignment = float(aligment.sum) / float(aligment.n)
    
    return table[len1][len2], meanAlignment

if __name__ == "__main__":
    s1 = "quickly"
    s2 = "qucehkly"
    msd, meanAlignment = MSD(s1, s2)
    print "MSD between \'%s\' and \'%s\': %d"%(s1, s2, msd)
    print "Mean aligment size: ", meanAlignment
    print "Error rate: ", msd / meanAlignment * 100.0

