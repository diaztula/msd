This code computes the minimum string distance between 2 strings, and also the mean size of 
all aligments between the two strings.
Implementation is based on the papers:
    1. "Measuring Errors in Text Entry Tasks: An Application of the Levenshtein String Distance Statistic"
    2. "A Character-level Error Analysis Technique for Evaluating Text Entry Methods"